{ pkgs ? import <nixpkgs> { } }: with pkgs;
let
  node = nodejs-16_x;
in
mkShell {
  shellHook = ''
    echo "Welcome to @jarvis-network/ui dev env"
    export NODE_PATH=$PWD/.nix-node
    export NPM_CONFIG_PREFIX=$PWD/.nix-node
    export PATH=$NODE_PATH/bin:$PATH
  '';
}
