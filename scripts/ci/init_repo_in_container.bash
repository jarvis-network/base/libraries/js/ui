#!/usr/bin/env bash


echo "Attempting to register a git GITLAB_TOKEN in automation"
echo

git config --global credential.helper store
echo "https://gitlab-ci-token:${CI_JOB_TOKEN}@gitlab.com" > ~/.git-credentials

git init "--initial-branch=$CI_COMMIT_REF_NAME"
git remote add origin "$CI_PROJECT_URL"
git fetch origin $CI_COMMIT_REF_NAME
git reset --hard "origin/$CI_COMMIT_REF_NAME"
