export interface InvertColorsOptions {
  background: string;
  primaryBackground: string;
  color: string;
  inverted?: boolean;
}

export interface CustomButtonOptions {
  background: string;
  hoverBackground: string;
  color: string;
  shadow: string;
  border: string;
}

export interface RoundedButtonOptions {
  borderRadius: string;
}

export interface DisabledButtonOptions {
  background: string;
  color: string;
}
