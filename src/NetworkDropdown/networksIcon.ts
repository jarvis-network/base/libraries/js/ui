import Eth from './networkLogo/mainnet.svg';
import BSC from './networkLogo/bsc.svg';
import Matic from './networkLogo/matic.svg';

export const networksIcon = {
  Eth,
  Matic,
  Mumbai: Matic,
  BSC,
};

export type NetworksIconKeys = keyof typeof networksIcon;
