import React, { useState } from 'react';

import { NetworkDropdown } from '..';
import { NetworkImageDropdownItem } from '../types';

export default {
  title: 'Dropdown/NetworkDropdown',
  component: NetworkDropdown,
};

export const Default = () => {
  const [active, setActive] = useState(1);
  const [isNetworkMenuVisible, setStateNetworkMenuVisible] = useState(false);
  const networkList: NetworkImageDropdownItem[] = [
    {
      networkId: 1,
      iconName: 'Eth',
      testnetIdentifier: '',
      onClick: () => {
        setActive(1);
        setStateNetworkMenuVisible(false);
      },
    },
    {
      networkId: 56,
      iconName: 'BSC',
      testnetIdentifier: '',
      onClick: () => {
        setActive(56);
        setStateNetworkMenuVisible(false);
      },
    },
    {
      networkId: 137,
      iconName: 'Matic',
      testnetIdentifier: '',
      onClick: () => {
        setActive(137);
        setStateNetworkMenuVisible(false);
      },
    },
    {
      networkId: 80001,
      iconName: 'Mumbai',
      testnetIdentifier: 'M',
      onClick: () => {
        setActive(80001);
        setStateNetworkMenuVisible(false);
      },
    },
  ];
  return (
    <NetworkDropdown
      size="s"
      position="absolute"
      selectedNetworkId={active}
      items={networkList}
      isExpanded={isNetworkMenuVisible}
      setExpanded={setStateNetworkMenuVisible}
    />
  );
};
