import React, { useContext, useMemo, useState } from 'react';

import { noop } from '../common/utils';

interface BackgroundHook {
  setOffset: (offset: string) => void;
  offset: string;
}

const BackgroundContext = React.createContext<BackgroundHook>({
  setOffset: noop,
  offset: '0',
});

export const BackgroundProvider: React.FC = ({ children }) => {
  const [state, setState] = useState<string>('0');
  const value = useMemo(
    () => ({
      setOffset: setState,
      offset: state,
    }),
    [setState, state],
  );
  return (
    <BackgroundContext.Provider value={value}>
      {children}
    </BackgroundContext.Provider>
  );
};

export const useBackground = () => useContext(BackgroundContext);
