import { activityIcon } from './icons/activity';
import { addCollateralIcon } from './icons/add-collateral';
import { closeCircleIcon } from './icons/close-circle';
import { switchIcon } from './icons/switch';
import { checkIcon } from './icons/check';
import { squareIcon } from './icons/square';
import { circleIcon } from './icons/circle';
import { successIcon } from './icons/success';
import { arrowRightIcon } from './icons/arrow-right';
import { searchIcon } from './icons/search';
import { originationFeeIcon } from './icons/origination-fee';
import { repayIcon } from './icons/repay';
import { removeCollateralIcon } from './icons/remove-collateral';
import { liquidationRatioIcon } from './icons/liquidation-ratio';
import { liquidationPriceIcon } from './icons/liquidation-price';
import { liquidationPenaltyIcon } from './icons/liquidation-penalty';
import { lightIcon } from './icons/light';
import { launchIcon } from './icons/launch';
import { jFIATCollateralPriceIcon } from './icons/jFIAT-collateral-price';
import { infoIcon } from './icons/info';
import { duskIcon } from './icons/dusk';
import { dotsIcon } from './icons/dots';
import { dropDownArrowIcon } from './icons/drop-down-arrow';
import { disconnectIcon } from './icons/disconnect';
import { debtIcon } from './icons/debt';
import { darkIcon } from './icons/dark';
import { copyIcon } from './icons/copy';
import { collateralRatioIcon } from './icons/collateral-ratio';
import { collateralIcon } from './icons/collateral';
import { closeIcon } from './icons/close';
import { borrowIcon } from './icons/borrow';
import { backArrowIcon } from './icons/back-arrow';

export const icons = {
  activityIcon,
  addCollateralIcon,
  backArrowIcon,
  borrowIcon,
  closeIcon,
  closeCircleIcon,
  collateralIcon,
  collateralRatioIcon,
  copyIcon,
  darkIcon,
  debtIcon,
  disconnectIcon,
  dropDownArrowIcon,
  dotsIcon,
  duskIcon,
  infoIcon,
  jFIATCollateralPriceIcon,
  launchIcon,
  lightIcon,
  liquidationPenaltyIcon,
  liquidationPriceIcon,
  liquidationRatioIcon,
  removeCollateralIcon,
  repayIcon,
  originationFeeIcon,
  searchIcon,
  arrowRightIcon,
  successIcon,
  circleIcon,
  squareIcon,
  checkIcon,
  switchIcon,
};

export type IconKeys = keyof typeof icons;
export const iconNames = Object.keys(icons) as IconKeys[];
