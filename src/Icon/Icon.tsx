import React from 'react';

import { styled } from '../Theme';

import { IconProps, Size } from './types';
import { icons } from './files';

const sizesMap = {
  small: 16,
  medium: 32,
  big: 48,
};

const getSize = (size: Size = 'small') => sizesMap[size];

const IconContainer = styled.i<IconProps>`
  width: ${props => getSize(props.size)}px;
  height: ${props => getSize(props.size)}px;
  color: ${props => props.color};
  display: inline-block;
`;

//
export const Icon: React.FC<IconProps> = ({ icon, color, size, ...props }) => {
  const iconComponent = icons[icon];

  if (!iconComponent || typeof iconComponent !== 'function') {
    return <></>;
  }

  return (
    <IconContainer color={color} icon={icon} size={size} {...props}>
      {iconComponent()}
    </IconContainer>
  );
};
