import React from 'react';

export const switchIcon = () => (
  <svg
    xmlns="http://www.w3.org/2000/svg"
    width="100%"
    height="100%"
    viewBox="0 0 13.816 17.058"
  >
    <g id="Switch" transform="translate(0.5 -0.547)">
      <path
        id="Line_6"
        data-name="Line 6"
        d="M28.421,52.1H17.207a.5.5,0,0,1,0-1H28.421a1.077,1.077,0,0,0,1.1-1.048V48.5a.5.5,0,0,1,1,0v1.548A2.078,2.078,0,0,1,28.421,52.1Z"
        transform="translate(-17.207 -37.589)"
        fill="currentColor"
      />
      <path
        id="Path_2224"
        data-name="Path 2224"
        d="M20.3,55.194a.5.5,0,0,1-.354-.146l-3.1-3.1a.5.5,0,0,1,0-.707l3.1-3.1a.5.5,0,1,1,.707.707L17.914,51.6l2.743,2.743a.5.5,0,0,1-.354.854Z"
        transform="translate(-17.207 -37.589)"
        fill="currentColor"
      />
      <path
        id="Line_6-2"
        data-name="Line 6"
        d="M17.207,47.1a.5.5,0,0,1-.5-.5V45.048A2.078,2.078,0,0,1,18.809,43H30.023a.5.5,0,1,1,0,1H18.809a1.077,1.077,0,0,0-1.1,1.048V46.6A.5.5,0,0,1,17.207,47.1Z"
        transform="translate(-17.207 -39.356)"
        fill="currentColor"
      />
      <path
        id="Path_2224-2"
        data-name="Path 2224"
        d="M26,46.194a.5.5,0,0,1-.354-.854L28.39,42.6l-2.743-2.743a.5.5,0,1,1,.707-.707l3.1,3.1a.5.5,0,0,1,0,.707l-3.1,3.1A.5.5,0,0,1,26,46.194Z"
        transform="translate(-16.281 -38.453)"
        fill="currentColor"
      />
    </g>
  </svg>
);
