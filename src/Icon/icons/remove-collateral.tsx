import React from 'react';

export const removeCollateralIcon = () => (
  <svg
    xmlns="http://www.w3.org/2000/svg"
    width="100%"
    height="100%"
    viewBox="0 0 25 25"
  >
    <g
      id="Remove_Collateral"
      data-name="Remove Collateral"
      transform="translate(24.5 24.5) rotate(180)"
    >
      <path
        id="Line_12"
        data-name="Line 12"
        d="M0,20.214a.5.5,0,0,1-.5-.5V0A.5.5,0,0,1,0-.5.5.5,0,0,1,.5,0V19.714A.5.5,0,0,1,0,20.214Z"
        transform="translate(12 4.286)"
        fill="currentColor"
      />
      <path
        id="Path_5066"
        data-name="Path 5066"
        d="M7.714,8.214a.5.5,0,0,1-.354-.146L-.354.354a.5.5,0,0,1,0-.707.5.5,0,0,1,.707,0L7.714,7.007,15.075-.354a.5.5,0,0,1,.707,0,.5.5,0,0,1,0,.707L8.068,8.068A.5.5,0,0,1,7.714,8.214Z"
        transform="translate(4.286 16.286)"
        fill="currentColor"
      />
      <path
        id="Line_13"
        data-name="Line 13"
        d="M24,.5H0A.5.5,0,0,1-.5,0,.5.5,0,0,1,0-.5H24a.5.5,0,0,1,.5.5A.5.5,0,0,1,24,.5Z"
        fill="currentColor"
      />
    </g>
  </svg>
);
