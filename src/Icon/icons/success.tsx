import React from 'react';

export const successIcon = () => (
  <svg
    id="Status"
    xmlns="http://www.w3.org/2000/svg"
    width="100%"
    height="100%"
    viewBox="0 0 15.999 16"
  >
    <path
      id="Path_2293"
      data-name="Path 2293"
      d="M7.59,10.544a.5.5,0,0,1-.353-.146L3.146,6.307A.5.5,0,0,1,3.852,5.6L7.527,9.276,14,.209a.5.5,0,0,1,.812.58L8,10.335a.5.5,0,0,1-.365.207Z"
      transform="translate(1.09 0.002)"
      fill="currentColor"
    />
    <path
      id="Path_2294"
      data-name="Path 2294"
      d="M8,16A8,8,0,1,1,10.908.546a.5.5,0,0,1-.363.93,7,7,0,1,0,4.34,5.251.5.5,0,0,1,.981-.181A8,8,0,0,1,8,16Z"
      transform="translate(-0.001 0.002)"
      fill="currentColor"
    />
  </svg>
);
