import React from 'react';

export const duskIcon = () => (
  <svg
    xmlns="http://www.w3.org/2000/svg"
    width="100%"
    height="100%"
    viewBox="0 0 16 13.42"
  >
    <g id="Group_1745" data-name="Group 1745" transform="translate(-1003 -43.54)">
      <path
        id="Path_2368"
        data-name="Path 2368"
        d="M3.234,12.029H16.356a.221.221,0,0,0,.234-.234v-.047A6.8,6.8,0,0,0,3,11.8.221.221,0,0,0,3.234,12.029Z"
        transform="translate(1001.205 38.54)"
        fill="currentColor"
      />
      <path
        id="Path_2369"
        data-name="Path 2369"
        d="M1337.736,1395.2h-15a.5.5,0,0,1,0-1h15a.5.5,0,0,1,0,1Z"
        transform="translate(-319.236 -1342.235)"
        fill="currentColor"
      />
      <path
        id="Path_2370"
        data-name="Path 2370"
        d="M1332.883,1395.2h-10.146a.5.5,0,0,1,0-1h10.146a.5.5,0,0,1,0,1Z"
        transform="translate(-316.809 -1340.235)"
        fill="currentColor"
      />
      <path
        id="Path_2371"
        data-name="Path 2371"
        d="M1328.788,1395.2h-6.052a.5.5,0,0,1,0-1h6.052a.5.5,0,0,1,0,1Z"
        transform="translate(-314.762 -1338.235)"
        fill="currentColor"
      />
    </g>
  </svg>
);
