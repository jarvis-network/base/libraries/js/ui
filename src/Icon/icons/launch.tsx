import React from 'react';

export const launchIcon = () => (
  <svg
    xmlns="http://www.w3.org/2000/svg"
    width="100%"
    height="100%"
    viewBox="0 0 17 17"
  >
    <g id="Launch" transform="translate(-4.5 -4.289)">
      <path
        id="Line_2"
        data-name="Line 2"
        d="M0,8.67a.5.5,0,0,1-.354-.146.5.5,0,0,1,0-.707l8.17-8.17a.5.5,0,0,1,.707,0,.5.5,0,0,1,0,.707L.354,8.523A.5.5,0,0,1,0,8.67Z"
        transform="translate(12.5 5.119)"
        fill="currentColor"
      />
      <path
        id="Path_72"
        data-name="Path 72"
        d="M42.483,10.983a.5.5,0,0,1-.5-.5V5.5H37a.5.5,0,0,1,0-1h5.484a.5.5,0,0,1,.5.5v5.483A.5.5,0,0,1,42.483,10.983Z"
        transform="translate(-21.483 -0.211)"
        fill="currentColor"
      />
      <path
        id="Path_73"
        data-name="Path 73"
        d="M18.238,26.278H6.539A2.042,2.042,0,0,1,4.5,24.238v-11.7A2.042,2.042,0,0,1,6.539,10.5h4.618a.5.5,0,0,1,0,1H6.539A1.041,1.041,0,0,0,5.5,12.539v11.7a1.041,1.041,0,0,0,1.039,1.039h11.7a1.041,1.041,0,0,0,1.039-1.039v-4.31a.5.5,0,0,1,1,0v4.31A2.042,2.042,0,0,1,18.238,26.278Z"
        transform="translate(0 -4.989)"
        fill="currentColor"
      />
    </g>
  </svg>
);
