import React, { FC, useState } from 'react';

import { AccountButton } from '../AccountButton';
import { Button } from '../Button';
import { flexRow } from '../common/mixins';
import { IconsDropdown, IconsDropdownItem } from '../IconsDropdown';
import { ThemeNameType, styled, useTheme } from '../Theme';
import { IconButton } from '../IconButton';

import { NetworkDropdown, NetworkImageDropdownItem } from '../NetworkDropdown';

import { Icon } from '../Icon';

import { MenuDropdown } from '../MenuDropdown';

import { AccountSummaryProps } from './types';

const Container = styled.div`
  ${flexRow()}
  height: 38px;
`;

const Item = styled.div<{ width: string }>`
  width: ${props => props.width};
  margin-left: 10px;
  border: 1px solid ${props => props.theme.border.primary};
  border-radius: ${props => props.theme.borderRadius.s};
  background: ${props => props.theme.background.secondary};

  :first-child {
    margin-left: 0;
  }
`;

const CustomAccountButton = styled(AccountButton)`
  border: none;
  font-size: ${props => props.theme.font.sizes.l};
`;

const FullHeightButton = styled(Button)`
  height: 100%;
  line-height: 100%;
  font-weight: 300;
  font-size: ${props => props.theme.font.sizes.l};
`;

const FullHeightError = styled(Button)`
  height: 100%;
  line-height: 100%;
  background-color: ${props => props.theme.common.danger};
  color: ${props => props.theme.common.white};
  display: flex;
  font-height: bold;
  font-size: ${props => props.theme.font.sizes.l};

  > :first-child {
    margin-right: 8px;
  }
`;

const FullWidthMenuDropdown = styled(MenuDropdown)`
  .dropdown-content {
    min-width: 100px;

    button {
      width: calc(100% - 20px) !important;
      margin: 0 10px !important;
      text-align: center;
    }
  }
`;

const CustomIconButton = styled(IconButton)`
  border-radius: ${props => props.theme.borderRadius.s};
  background: ${props => props.theme.background.secondary};
  padding: 0;
  i {
    width: 100%;
  }
  svg {
    width: 18px;
    height: 14px;
  }
`;

export const AccountSummary: FC<AccountSummaryProps> = ({
  name,
  networkList,
  wallet,
  image,
  selectedNetworkId,
  onLogin,
  onAccount,
  settingMenu,
  onThemeChange,
  contentOnTop = false,
  authLabel = 'Connect',
  isUnsupportedNetworkId,
}) => {
  const { name: theme } = useTheme();

  const [isSettingMenuVisible, setStateSettingMenuVisible] = useState(false);
  const [isNetworkMenuVisible, setStateNetworkMenuVisible] = useState(false);
  const [isThemeSwitcherVisible, setThemeSwitcherVisible] = useState(false);

  const setSettingMenuVisible = (state: boolean) => {
    setStateSettingMenuVisible(state);
  };

  const handleThemeChange = (newTheme: ThemeNameType) => {
    if (!onThemeChange) {
      return;
    }

    setThemeSwitcherVisible(false);
    onThemeChange(newTheme);
  };

  const settingsMenuItems = settingMenu
    ? settingMenu.map(item => {
        if ('onClick' in item) {
          return {
            ...item,
            onClick: () => {
              item.onClick();
              setSettingMenuVisible(false);
            },
          };
        }
        return item;
      })
    : undefined;

  const networkMenuItems = networkList
    ? networkList.reduce(
        (acc: NetworkImageDropdownItem[], ele: NetworkImageDropdownItem) => {
          if ('onClick' in ele) {
            return [
              ...acc,
              {
                ...ele,
                onClick: () => {
                  ele.onClick();
                  setStateNetworkMenuVisible(false);
                },
              },
            ];
          }
          return [...acc, { ...ele }];
        },
        [],
      )
    : undefined;

  const themes: (IconsDropdownItem & { theme: ThemeNameType })[] | null =
    onThemeChange
      ? [
          {
            theme: 'light',
            icon: 'lightIcon',
            onClick: () => handleThemeChange('light'),
          },
          {
            theme: 'dusk',
            icon: 'duskIcon',
            onClick: () => handleThemeChange('dusk'),
          },
          {
            theme: 'dark',
            icon: 'darkIcon',
            onClick: () => handleThemeChange('dark'),
          },
        ]
      : null;

  const auth = isUnsupportedNetworkId ? (
    <FullHeightError block size="l" type="transparent">
      <Icon icon="activityIcon" /> Wrong Network
    </FullHeightError>
  ) : wallet ? (
    <CustomAccountButton
      image={image}
      name={name}
      wallet={wallet}
      onClick={onAccount}
    />
  ) : (
    <FullHeightButton onClick={onLogin} block size="l" type="transparent">
      {authLabel}
    </FullHeightButton>
  );

  const settingHeader = (
    <CustomIconButton
      size="s"
      icon="dotsIcon"
      onClick={() => setStateSettingMenuVisible(true)}
    />
  );

  return (
    <Container>
      {!isUnsupportedNetworkId && selectedNetworkId && (
        <Item width="37px">
          <NetworkDropdown
            size="s"
            position="absolute"
            contentOnTop={contentOnTop || false}
            selectedNetworkId={selectedNetworkId}
            items={networkMenuItems}
            isExpanded={isNetworkMenuVisible}
            setExpanded={setStateNetworkMenuVisible}
          />
        </Item>
      )}

      <Item width="100%">{auth}</Item>

      {themes && (
        <Item width="37px">
          <IconsDropdown
            size="s"
            position="absolute"
            contentOnTop={contentOnTop || false}
            active={themes.findIndex(i => i.theme === theme)}
            items={themes}
            isExpanded={isThemeSwitcherVisible}
            setExpanded={setThemeSwitcherVisible}
          />
        </Item>
      )}

      {settingMenu && (
        <Item width="37px">
          <FullWidthMenuDropdown
            size="m"
            position="absolute"
            contentOnTop={contentOnTop || false}
            header={settingHeader}
            items={settingsMenuItems}
            isExpanded={isSettingMenuVisible}
            setExpanded={setSettingMenuVisible}
          />
        </Item>
      )}
    </Container>
  );
};
