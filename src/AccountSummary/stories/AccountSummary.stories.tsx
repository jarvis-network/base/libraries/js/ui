import React, { useState } from 'react';

import { action } from '@storybook/addon-actions';

import { ColoredBorderPanel } from '../../ColoredBorderPanel';

import { AccountSummary } from '..';

import { NetworkImageDropdownItem } from '../../NetworkDropdown';

export default {
  title: 'Account/AccountSummary',
  component: AccountSummary,
  argTypes: {
    name: {
      control: false,
    },
    wallet: {
      control: false,
    },
    image: {
      control: false,
    },
    accountMenu: {
      control: false,
    },
    settingMenu: {
      control: false,
    },
    networkList: {
      control: false,
    },
    selectedNetworkId: {
      control: false,
    },
    className: {
      control: false,
    },
  },
};

const networkList: NetworkImageDropdownItem[] = [
  {
    networkId: 1,
    iconName: 'Eth',
    testnetIdentifier: '',
    onClick: () => {},
  },
  {
    networkId: 56,
    iconName: 'BSC',
    testnetIdentifier: '',
    onClick: () => {},
  },
  {
    networkId: 137,
    iconName: 'Matic',
    testnetIdentifier: '',
    onClick: () => {},
  },
  {
    networkId: 80001,
    iconName: 'Mumbai',
    testnetIdentifier: 'M',
    onClick: () => {},
  },
];

const settings = [
  {
    name: 'News',
    key: 'News',
    onClick: () =>
      window.open('https://issue.jarvis.network/', '_blank', 'noopener'),
  },
  {
    name: 'Documentation',
    key: 'Documentation',
    onClick: () =>
      window.open('https://learn.jarvis.network/', '_blank', 'noopener'),
  },
  {
    name: 'Blog',
    key: 'Blog',
    onClick: () =>
      window.open('https://medium.com/@jarvisnetwork', '_blank', 'noopener'),
  },
  {
    name: 'Forum',
    key: 'Forum',
    onClick: () =>
      window.open('https://forum.jarvis.network/', '_blank', 'noopener'),
  },
  {
    name: 'Discord',
    key: 'Discord',
    onClick: () =>
      window.open('https://discord.com/invite/YSPpBWH6fa', '_blank', 'noopener'),
  },
  {
    name: 'Telegram',
    key: 'Telegram',
    onClick: () => window.open('https://t.me/jarvisnetwork', '_blank', 'noopener'),
  },
];

export const Default = () => {
  const [selectedNetworkId, setSelectedNetworkId] = useState(1);
  const networks = networkList.map(network => ({
    ...network,
    onClick: () => {
      action('Network selected')(`${network.iconName}`);
      setSelectedNetworkId(network.networkId);
    },
  }));
  return (
    <AccountSummary
      onAccount={action('Open account modal')}
      wallet="0x235c..fe47"
      selectedNetworkId={selectedNetworkId}
      networkList={networks}
      onThemeChange={theme => action('theme selected')(theme)}
      settingMenu={settings}
    />
  );
};

export const ContentOnTop = () => {
  const [selectedNetworkId, setSelectedNetworkId] = useState(1);
  const networks = networkList.map(network => ({
    ...network,
    onClick: () => {
      action('Network selected')(`${network.iconName}`);
      setSelectedNetworkId(network.networkId);
    },
  }));
  return (
    <ColoredBorderPanel>
      <p>Interactive demo:</p>
      <AccountSummary
        onAccount={action('Open account modal')}
        contentOnTop
        wallet="0x235c..fe47"
        selectedNetworkId={selectedNetworkId}
        networkList={networks}
        onThemeChange={theme => action('theme selected')(theme)}
        settingMenu={settings}
      />
    </ColoredBorderPanel>
  );
};

export const WrongNetwork = () => {
  const [selectedNetworkId, setSelectedNetworkId] = useState(1);
  const networks = networkList.map(network => ({
    ...network,
    onClick: () => {
      action('Network selected')(`${network.iconName}`);
      setSelectedNetworkId(network.networkId);
    },
  }));
  return (
    <AccountSummary
      isUnsupportedNetworkId
      onAccount={action('Open account modal')}
      wallet="0x235c..fe47"
      selectedNetworkId={selectedNetworkId}
      networkList={networks}
      onThemeChange={theme => action('theme selected')(theme)}
      settingMenu={settings}
    />
  );
};

export const SignIn = () => {
  const [selectedNetworkId, setSelectedNetworkId] = useState(1);
  const networks = networkList.map(network => ({
    ...network,
    onClick: () => {
      action('Network selected')(`${network.iconName}`);
      setSelectedNetworkId(network.networkId);
    },
  }));
  return (
    <AccountSummary
      onAccount={action('Open account modal')}
      selectedNetworkId={selectedNetworkId}
      networkList={networks}
      onLogin={action('Login action')}
      onThemeChange={theme => action('theme selected')(theme)}
      settingMenu={settings}
    />
  );
};

export const Custom = ({
  isLogin,
  isUnsupportedNetworkId,
  wallet,
  authLabel,
  contentOnTop,
}: {
  isLogin: boolean;
  isUnsupportedNetworkId: boolean;
  wallet: string;
  authLabel: string;
  contentOnTop: boolean;
}) => {
  const [selectedNetworkId, setSelectedNetworkId] = useState(1);
  const networks = networkList.map(network => ({
    ...network,
    onClick: () => {
      action('Network selected')(`${network.iconName}`);
      setSelectedNetworkId(network.networkId);
    },
  }));
  return (
    <AccountSummary
      isUnsupportedNetworkId={isUnsupportedNetworkId}
      onAccount={action('Open account modal')}
      selectedNetworkId={selectedNetworkId}
      wallet={isLogin ? wallet : ''}
      networkList={networks}
      onLogin={action('Login action')}
      onThemeChange={theme => action('theme selected')(theme)}
      settingMenu={settings}
      authLabel={authLabel}
      contentOnTop={contentOnTop}
    />
  );
};

Custom.args = {
  isLogin: true,
  isUnsupportedNetworkId: false,
  wallet: '0x235c..fe47',
  authLabel: 'Connect',
  contentOnTop: false,
};
