import { useTheme } from '../Theme';

import { useWindowSize } from './useWindowSize';

export const useIsMobile = (): boolean => {
  const theme = useTheme();
  return useWindowSize().innerWidth <= theme.rwd.breakpoints[theme.rwd.mobileIndex];
};
