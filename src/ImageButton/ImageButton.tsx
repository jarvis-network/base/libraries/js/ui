import React from 'react';

import { styled } from '../Theme';

import { Button } from '../Button';

import {
  ImageButtonContainerProps,
  ImageButtonProps,
  Padding,
  Size,
} from './types';

const IconContainer = styled.div`
  display: inherit;
  position: relative;
`;

const TestnetIdentifier = styled.span`
  width: 12px;
  height: 12px;
  top: -4px;
  right: -7px;
  font-size: 8px;
  background: rgb(143, 101, 255);
  color: rgb(255, 255, 255);
  position: absolute;
  display: flex;
  justify-content: center;
  border-radius: 50%;
  z-index: 10;
  item-align: center;
`;
const ImageButtonContainer: React.FC<ImageButtonContainerProps> = ({
  children,
  ...options
}) => {
  const { size = 'xl' } = options;

  const Component = styled(Button)`
    display: flex;
    justify-content: center;
    font-size: ${props => props.theme.font.sizes[size]};
    padding: ${Padding[size]}px;
    height: ${Size[size]}px;
    width: ${Size[size]}px;
    border-radius: ${props => props.theme.borderRadius.s};
  `;

  return <Component {...options}>{children}</Component>;
};

const InlineImageButtonContainer: React.FC<ImageButtonContainerProps> = ({
  children,
  type = 'transparent',
  ...options
}) => {
  const { size = 'l' } = options;

  const Component = styled(Button)`
    display: inline-flex;
    vertical-align: bottom;
    font-size: ${props => props.theme.font.sizes[size]};
    height: ${Size[size] / 2}px;
    width: ${Size[size] / 2 + Padding[size] / 2}px;
    padding: 0;
  `;

  return (
    <Component {...options} type={type}>
      {children}
    </Component>
  );
};

export const ImageButton: React.FC<ImageButtonProps> = ({
  src,
  alt,
  width = 15,
  height = 24,
  testnetIdentifier,
  className,
  inline,
  ...props
}) => {
  const Component = inline ? InlineImageButtonContainer : ImageButtonContainer;

  return (
    <Component className={className || ''} {...props}>
      <IconContainer>
        <img alt={alt} src={src} width={width} height={height} />
        {testnetIdentifier && (
          <TestnetIdentifier>{testnetIdentifier}</TestnetIdentifier>
        )}
      </IconContainer>
    </Component>
  );
};
