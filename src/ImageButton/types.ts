import { ReactNode } from 'react';

import { AllButtonProps } from '../Button/types';

export interface ImageButtonProps extends AllButtonProps {
  src: string;
  testnetIdentifier?: string;
  alt: string;
  inline?: boolean;
  width?: number;
  height?: number;
}

export interface ImageButtonContainerProps extends AllButtonProps {
  children: ReactNode;
}

export enum Padding {
  xxs = 4,
  xs = 5,
  s = 6,
  m = 7,
  l = 8,
  xl = 9,
  xxl = 10,
  xxxl = 11,
}

export enum Size {
  xxs = 25,
  xs = 30,
  s = 35,
  m = 40,
  l = 45,
  xl = 50,
  xxl = 55,
  xxxl = 60,
}
