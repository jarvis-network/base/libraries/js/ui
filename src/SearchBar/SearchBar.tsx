import React, { useState } from 'react';

import { styled, useTheme } from '../Theme';

import { Input } from '../Input';
import { DataRows } from '../DataGrid';

import { Icon } from '../Icon';

import { PropFilterWithFilterPropFn, SearchBarProps } from './types';

const propFilter: PropFilterWithFilterPropFn = (
  data,
  { queryFilterProp, query, currentTabValue, tabFilterProp },
) => {
  const q = query.toLowerCase();

  return data.filter(item => {
    if (
      query &&
      queryFilterProp &&
      !item[queryFilterProp].toLowerCase().includes(q)
    ) {
      return false;
    }

    if (
      currentTabValue &&
      tabFilterProp &&
      item[tabFilterProp] !== currentTabValue
    ) {
      return false;
    }

    return true;
  });
};

const Container = styled.div`
  height: 100%;
  width: 100%;
`;

export const SearchBar: React.FC<SearchBarProps> = ({
  tabs,
  data,
  queryFilterProp,
  className,
  render,
  filter,
  ...inputProps
}) => {
  const theme = useTheme();
  const [stateQuery, setStateQuery] = useState('');
  const [displayPrefix, setDisplayPrefix] = useState(true);
  const query = 'value' in inputProps ? inputProps.value || '' : stateQuery;

  const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setStateQuery(event.currentTarget.value);
    if (event.target.value.length > 0) {
      setDisplayPrefix(false);
    } else {
      setDisplayPrefix(true);
    }
    inputProps.onChange?.(event);
  };

  const currentTabValue =
    (typeof tabs?.selected === 'number' && tabs.tabs[tabs.selected]?.filterValue) ||
    '';
  const tabFilterProp = tabs?.filterProp;

  const prefix = (
    <Icon
      icon="searchIcon"
      className="icon"
      noSpaceAround
      style={{ color: theme.text.disabled }}
    />
  );
  const suffix = (
    <Icon
      icon="closeCircleIcon"
      style={{ color: theme.text.primary }}
      className="icon"
      noSpaceAround
      onClick={() => {
        setStateQuery('');
        setDisplayPrefix(true);
      }}
    />
  );

  const filteredData: DataRows<any>[] = filter
    ? filter(data || [], { query, currentTabValue, tabFilterProp })
    : propFilter(data || [], {
        queryFilterProp,
        query,
        currentTabValue,
        tabFilterProp,
      });

  return (
    <Container className={className || ''}>
      <Input
        prefix={displayPrefix ? prefix : undefined}
        suffix={!displayPrefix ? suffix : undefined}
        {...inputProps}
        value={query}
        isSearch
        onChange={handleChange}
      />
      {render &&
        render({
          filteredData,
          query,
        })}
    </Container>
  );
};
