import React from 'react';

import { OnTablet } from '../OnTablet';

export default {
  title: 'common/OnTablet',
  component: OnTablet,
};

export const Basic = () => (
  <div>
    On tablet device (resolution) you will see a cat emoji here:
    <OnTablet>🐈</OnTablet>
  </div>
);
