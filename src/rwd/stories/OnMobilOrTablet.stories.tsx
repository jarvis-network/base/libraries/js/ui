import React from 'react';

import { OnMobilOrTablet } from '../OnMobilOrTablet';

export default {
  title: 'common/OnMobilOrTablet',
  component: OnMobilOrTablet,
};

export const Basic = () => (
  <div>
    On mobile or tablet device (resolution) you will see a dog emoji here:
    <OnMobilOrTablet>🐕</OnMobilOrTablet>
  </div>
);
