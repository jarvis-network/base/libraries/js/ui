import React from 'react';

import { styled } from '../Theme';

interface Props {
  items: any[];
  onChange: (item: any) => void;
  selected: number;
}

const Wrapper = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
`;

const Container = styled.div`
  border: 1px solid ${props => props.theme.border.primary};
  border-radius: 14px;
  height: 36px;
  padding: 4px;
  background-color: ${props => props.theme.background.secondary};
`;

const Button = styled.button<{ isActive: boolean }>`
  text-transform: capitalize;
  background: ${props =>
    props.isActive
      ? props.theme.background.active
      : props.theme.background.secondary};
  border: none;
  border-radius: 10px;
  min-width: 60px;
  height: 100%;
  color: ${props =>
    props.isActive ? props.theme.text.inverted : props.theme.text.primary};
  outline: none;
`;

export const Switcher: React.FC<Props> = ({ items, onChange, selected }) => (
  <Wrapper>
    <Container>
      {items.map((value, key) => (
        <Button
          key={value}
          onClick={() => onChange(value)}
          isActive={selected === key}
          type="button"
          className={selected === key ? 'active' : ''}
        >
          {value}
        </Button>
      ))}
    </Container>
  </Wrapper>
);
