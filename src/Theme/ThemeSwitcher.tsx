import React, { FC } from 'react';

import { useTheme } from '@emotion/react';

import { Switcher } from '../Switcher';

import { ThemeSwitcherProps, ThemesList } from './types';

export const ThemeSwitcher: FC<ThemeSwitcherProps> = ({ setTheme }) => {
  const theme = useTheme();

  return (
    <Switcher
      items={ThemesList}
      onChange={setTheme}
      selected={ThemesList.indexOf(theme.name)}
    />
  );
};
