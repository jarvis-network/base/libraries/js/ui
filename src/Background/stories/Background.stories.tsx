import React from 'react';

import { styled } from '../../Theme';

import { Background } from '../Background';
import { images } from '../../AnimatedBackground/stories/assets';

export default {
  title: 'background/Background',
  component: Background,
};

const Container = styled.div`
  height: 500px;
  width: 500px;
`;

export const BasicBackground = () => (
  <Container>
    <Background image={images['./desert_background.svg']} />
  </Container>
);
