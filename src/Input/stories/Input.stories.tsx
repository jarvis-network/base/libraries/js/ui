import React, { useState } from 'react';
import { boolean, number, select, text } from '@storybook/addon-knobs';
import { action } from '@storybook/addon-actions';

import { styled } from '../../Theme';

import { InputProps } from '../types';

import { Emoji } from '../../Emoji';
import { emojiList } from '../../Emoji/stories/data';

import { Input } from '..';

export default {
  title: 'Input',
  component: Input,
};

interface StatefulInputProps extends InputProps {
  initialValue?: string;
}

const Container = styled.div<{ width: number }>`
  width: ${props => props.width}%;
`;

const StatefulInput: React.FC<StatefulInputProps> = ({
  initialValue = '',
  ...props
}) => {
  const [value, setValue] = useState<string>(initialValue);

  return (
    <Input
      value={value}
      onChange={(e: React.ChangeEvent<HTMLInputElement>) => {
        setValue(e.target.value);
        action(`input text changed: ${e.target.value}`);
      }}
      {...props}
    />
  );
};

export const Default = () => <StatefulInput />;

export const WithInitialValue = () => (
  <StatefulInput initialValue="Initial value" />
);

export const WithPlaceholder = () => (
  <StatefulInput placeholder="This is a placeholder" />
);

export const Invalid = () => (
  <StatefulInput isInvalid initialValue="Invalid value" />
);

export const InvalidWithInfo = () => (
  <StatefulInput isInvalid initialValue="Invalid value" info="Wrong value !" />
);

export const WithPrefix = () => (
  <StatefulInput
    prefix={<Emoji emoji="MoneyMouth" />}
    initialValue="Initial value"
  />
);

export const WithSuffix = () => (
  <StatefulInput
    suffix={<Emoji emoji="MoneyMouth" />}
    initialValue="Initial value"
  />
);

export const CustomWidth = () => {
  const width = number('Width %', 100, {
    range: true,
    min: 1,
    max: 100,
    step: 1,
  });
  return (
    <Container width={width}>
      <StatefulInput placeholder="This is a placeholder" />
    </Container>
  );
};

export const WithInfo = () => (
  <StatefulInput info="Info" initialValue="Initial value" />
);

export const Knobs = () => {
  const enablePrefix = boolean('Prefix', false);
  let prefix;
  if (enablePrefix) {
    prefix = <Emoji emoji={select('Prefix emoji name', emojiList, 'MoneyMouth')} />;
  }

  const enableSuffix = boolean('Suffix', false);
  let suffix;
  if (enableSuffix) {
    suffix = <Emoji emoji={select('Suffix emoji name', emojiList, 'MoneyMouth')} />;
  }

  return (
    <StatefulInput
      placeholder={text('Placeholder', 'Placeholder')}
      value={text('Value', '')}
      isInvalid={boolean('Invalid', false)}
      invalidMessage={text('Invalid Message', 'Some error message')}
      info={text('Info message', '')}
      prefix={prefix}
      suffix={suffix}
    />
  );
};
