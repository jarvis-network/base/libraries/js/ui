import React from 'react';

import { styled } from '../Theme';

import { ListViewItem } from './ListViewItem';

export interface ListViewProps {
  className?: string;
  itemsConfig?: {
    items?: any[];
    key?: string;
    display?: string;
  };
}

const ListViewContainer = styled.div`
  background: ${props => props.theme.background.secondary};
  overflow: hidden;
  border-radius: ${props => props.theme.borderRadius.m};
`;

const getItemsConfig = (itemsConfig: {
  items?: any[];
  key?: string;
  display?: string;
}) => {
  if (!itemsConfig.items && !Array.isArray(itemsConfig.items)) {
    return null;
  }

  const config = {
    key: 'id',
    display: 'name',
    ...itemsConfig,
  };

  if (typeof itemsConfig.items[0] === 'object') {
    return config;
  }

  return {
    ...config,
    items: config.items?.map((value, id) => ({
      [config.key]: id,
      [config.display]: value,
    })),
  };
};

export const ListView: React.FC<ListViewProps> = ({
  children,
  className,
  itemsConfig,
  ...props
}) => {
  if (itemsConfig && itemsConfig.items) {
    const config = getItemsConfig(itemsConfig);

    if (!config) {
      throw new Error(
        'Your itemsConfig property is invalid. Please check your itemsConfig.',
      );
    }

    return (
      <ListViewContainer className={className || ''} {...props}>
        {config.items?.map(i => (
          <ListViewItem key={i.id}>{i.name}</ListViewItem>
        ))}
      </ListViewContainer>
    );
  }

  return (
    <ListViewContainer className={className || ''} {...props}>
      {children}
    </ListViewContainer>
  );
};
