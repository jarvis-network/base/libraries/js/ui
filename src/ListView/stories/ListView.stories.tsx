import React from 'react';

import { styled, ThemeProvider } from '../../Theme';

import { ListView, ListViewItem } from '..';
import { Icon } from '../../Icon';
import { Flag } from '../../Flag';
import { IconButton } from '../../IconButton';

export default {
  title: 'ListView',
  component: ListView,
};

const items = [
  {
    id: 1,
    name: 'first item',
  },
  {
    id: 2,
    name: 'second item',
  },
  {
    id: 3,
    name: 'third item',
  },
];

export const Default = () => (
  <ThemeProvider>
    <ListView>
      {items.map(i => (
        <ListViewItem key={i.id}>{i.name}</ListViewItem>
      ))}
    </ListView>
  </ThemeProvider>
);

export const NoListViewItemsObjectArray = () => (
  <ListView
    itemsConfig={{
      items,
    }}
  />
);

export const NoListViewItemsSimpleArray = () => (
  <ListView
    itemsConfig={{
      items: items.map(i => i.name),
    }}
  />
);

const history = [
  {
    id: 1,
    date: 'Jan 04',
    transactions: [
      {
        id: 1,
        currency: 'jEUR',
        value: 50,
        operation: 'borrowed',
        time: '12:41 PM',
        inUSD: '$56.47',
      },
      {
        id: 2,
        currency: 'jCHF',
        value: 43,
        operation: 'borrowed',
        time: '08:10 PM',
        inUSD: '$56.47',
        collateral: 'USDC',
        collateralValue: 96,
        collateralInUSD: '$96',
      },
    ],
  },
  {
    id: 2,
    date: 'Dec 19',
    transactions: [
      {
        id: 1,
        currency: 'jEUR',
        value: 50,
        operation: 'borrowed',
        time: '11:01 PM',
        inUSD: '$56.47',
        collateral: 'USDC',
        collateralValue: 100,
        collateralInUSD: '$100',
      },
    ],
  },
];

const TransactionDetails = styled.div`
  display: flex;
  gap: 5px;
  align-items: center;
`;

const TransactionCurrencyWrapper = styled.div`
  .in-usd {
    font-size: 14px;
    color: ${props => props.theme.text.disabled};
  }
`;

const StyledDay = styled(ListViewItem)`
  margin-bottom: 24px;
  margin-left: 16px;
`;

const TransactionOperation = styled.div`
  display: flex;
  justify-content: space-between;

  .transaction-actions {
    display: flex;
    align-items: center;
    justify-content: center;
    gap: 20px;
  }
`;

export const exampleHistory = () => {
  const renderTransactionCollateral = (transaction: any) => (
    <TransactionDetails>
      <Flag flag={transaction.collateral} />
      <TransactionCurrencyWrapper>
        <div>
          +{transaction.collateralValue} {transaction.collateral}
        </div>
        <div className="in-usd">+{transaction.collateralInUSD}</div>
      </TransactionCurrencyWrapper>
    </TransactionDetails>
  );

  const renderTransactions = (transactions: any[], keyPrefix: string) => (
    <ListView>
      {transactions.map(t => (
        <ListViewItem key={`${keyPrefix}_t-${t.id}`}>
          <TransactionOperation>
            <div className="transaction-info">
              <Icon icon="borrowIcon" /> {t.operation} {t.currency} {t.time}
            </div>
            <div className="transaction-actions">
              <Icon icon="successIcon" size="small" />

              <IconButton icon="launchIcon" size="m" />
            </div>
          </TransactionOperation>
          <TransactionDetails>
            <Flag flag={t.currency} />
            <TransactionCurrencyWrapper>
              <div>
                +{t.value} {t.currency}
              </div>
              <div className="in-usd">+{t.inUSD}</div>
            </TransactionCurrencyWrapper>

            {t.collateral ? renderTransactionCollateral(t) : ''}
          </TransactionDetails>
        </ListViewItem>
      ))}
    </ListView>
  );

  const renderDay = (day: any) => {
    const key = `d-${day.id}`;

    return (
      <StyledDay key={key}>
        <span>{day.date}</span>
        {renderTransactions(day.transactions, key)}
      </StyledDay>
    );
  };

  return <ListView>{history.map(day => renderDay(day))}</ListView>;
};
