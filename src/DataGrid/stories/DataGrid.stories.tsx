import React from 'react';

import { CellInfo } from 'react-table';

import { action } from '@storybook/addon-actions';

import { styled } from '../../Theme';

import { DataGridColumnProps, ColumnType } from '../types';

import { DataGrid } from '..';

import { Flag } from '../../Flag';

import { Emoji } from '../../Emoji';

import { Icon } from '../../Icon';

import { StandardButton } from '../../Button/variants';

import { IconButton } from '../../IconButton';

import { data, loansMockedData } from './data';

export default {
  title: 'DataGrid',
  component: DataGrid,
};

const columns: DataGridColumnProps[] = [
  {
    header: 'Ticket',
    key: 'col1',
    type: ColumnType.Text,
  },
  {
    header: 'Symbol',
    key: 'col2',
    type: ColumnType.Text,
  },
  {
    header: 'Profit (DAI)',
    key: 'col3',
    type: ColumnType.RangeColor,
    downColor: 'blue',
    upColor: 'red',
  },
];

export const Default = () => <DataGrid columns={columns} data={data} />;

const LoanIconsWrapper = styled.div`
  position: relative;

  .debt-currency {
    position: absolute;
    margin-left: -15px;
  }

  @media screen and (max-width: ${props =>
      props.theme.rwd.breakpoints[props.theme.rwd.desktopIndex - 1]}px) {
    margin-right: 10px;

    img {
      width: 21px;
    }

    .debt-currency {
      margin-left: -12px;
    }
  }
`;

const YourRatio = styled.div`
  position: relative;
  color: green;

  @media screen and (max-width: ${props =>
      props.theme.rwd.breakpoints[props.theme.rwd.desktopIndex - 1]}px) {
    display: flex;
    justify-content: center;
    align-items: center;
    gap: 3px;
    margin-left: 15px;

    .emoji {
      font-size: 9px;
    }
  }
`;

const HeaderHelper = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  gap: 5px;
`;

const CustomValue = styled.div`
  p {
    margin: 0;
  }

  .in-dollars {
    color: ${props => props.theme.text.secondary};
    @media screen and (max-width: ${props =>
        props.theme.rwd.breakpoints[props.theme.rwd.desktopIndex - 1]}px) {
      display: none;
    }
  }
`;

const loanTableCols: DataGridColumnProps[] = [
  {
    header: 'Loan',
    key: 'loan',
    type: ColumnType.CustomCell,
    cell: (cellData: CellInfo) => (
      <LoanIconsWrapper>
        <Flag flag={cellData.original.collateral} />
        <Flag className="debt-currency" flag={cellData.original.debt} />
      </LoanIconsWrapper>
    ),
  },
  {
    header: 'Debt',
    key: 'debtValue',
    type: ColumnType.CustomCell,
    cell: cellData => (
      <CustomValue>
        <p>{cellData.original.debtValue}</p>
        <p className="in-dollars">$56.00</p>
      </CustomValue>
    ),
  },
  {
    header: 'Collateral',
    key: 'collateralValue',
    type: ColumnType.CustomCell,
    hideOnMobile: true,
    cell: cellData => (
      <CustomValue>
        <p>{cellData.original.collateralValue}</p>
        <p className="in-dollars">$100</p>
      </CustomValue>
    ),
  },
  {
    header: () => (
      <HeaderHelper>
        Liquidation Ratio
        <Icon icon="infoIcon" color="#00f" size="small" />
      </HeaderHelper>
    ),
    key: 'liquidationRatio',
    hideOnMobile: true,
    type: ColumnType.Text,
  },
  {
    header: () => (
      <HeaderHelper>
        Your Liquidation <Icon icon="infoIcon" color="#00f" size="small" />
      </HeaderHelper>
    ),
    key: 'currentRatio',
    type: ColumnType.CustomCell,
    cell: (cellData: CellInfo) => (
      <YourRatio>
        <Emoji emoji="MoneyMouth" className="emoji" />{' '}
        {cellData.original.currentRatio}%
      </YourRatio>
    ),
  },
  {
    header: '',
    key: 'modify',
    type: ColumnType.CustomCell,
    hideOnMobile: true,
    cell: (cellData: CellInfo) => (
      <StandardButton
        buttonType="secondary"
        size="l"
        onClick={() => action(`Custom Cell Edit${cellData.original}`)}
      >
        Manage
      </StandardButton>
    ),
  },
];

const loanTableData = loansMockedData.map(row => ({
  ...row,
  debtValue: `${row.debtValue} ${row.debt}`,
  collateralValue: `${row.collateralValue} ${row.collateral}`,
  liquidationRatio: `${row.liquidationRatio} %`,
}));

export const DesktopExample = () => (
  <DataGrid columns={loanTableCols} data={loanTableData} />
);

const StyledIconButton = styled(IconButton)`
  i {
    width: 14px;
  }
`;

const mobileLoans = [
  ...loanTableCols
    .filter(c => !c.hideOnMobile)
    .map(c => ({
      ...c,
      header: '',
    })),
  {
    header: '',
    key: 'modify',
    type: ColumnType.CustomCell,
    cell: (cellData: CellInfo) => (
      <StyledIconButton
        size="l"
        onClick={() => {
          console.log('manage', cellData);
          return action(`Custom Cell Edit${cellData.original}`);
        }}
        icon="arrowRightIcon"
      >
        Manage
      </StyledIconButton>
    ),
  },
];

export const MobileExample = () => (
  <DataGrid columns={mobileLoans} data={loanTableData} />
);
