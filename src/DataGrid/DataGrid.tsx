import React from 'react';

import ReactTable, { CellInfo, TableProps } from 'react-table';

import { styled } from '../Theme';

import { ColumnType, DataGridColumnProps } from './types';
import DataGridPagination from './DataGridPagination';
import IconCell, { IconCellProps } from './ColumnTypes/icon';
import RangeCell, { RangeCellProps } from './ColumnTypes/rangeColor';

const TableContainer = styled(ReactTable)`
  background: ${props => props.theme.background.primary};

  .blue {
    color: #00b0f0;
  }

  .red {
    color: ${props => props.theme.common.danger};
  }

  .table-icon {
    width: 12px;
  }

  .icon-btn {
    background: transparent;
    border: 0;
    outline: none;
  }

  .rt-td {
    font-size: ${props => props.theme.font.sizes.m};
    color: ${props => props.theme.text.primary};

    @media screen and (max-width: ${props =>
        props.theme.rwd.breakpoints[props.theme.rwd.desktopIndex - 1]}px) {
      width: max-content !important;
      padding: 0 !important;
      height: 70px;
    }
  }

  .rt-tbody {
    @media screen and (max-width: ${props =>
        props.theme.rwd.breakpoints[props.theme.rwd.desktopIndex - 1]}px) {
      min-width: auto !important;
      background: red !important;
      display: grid !important;
    }
  }

  .rt-th,
  .rt-td {
    display: flex;
    align-items: center;
    justify-content: center;
  }

  .rt-table,
  .rt-td {
    background: ${props => props.theme.background.secondary};
  }

  .rt-table {
    border-radius: ${props => props.theme.borderRadius.m};
    overflow: hidden;
  }

  .rt-tbody,
  .rt-tr {
    background: ${props => props.theme.background.secondary};
    display: flex;
  }

  .rt-resizable-header {
    box-shadow: none !important;

    :before {
      position: absolute;
      left: 50%;
      margin-left: -15px;
      width: 30px;
      border-bottom: 3px solid ${props => props.theme.common.success};
    }

    &.-sort-asc:before {
      content: '';
      bottom: 0;
    }

    &.-sort-desc:before {
      content: '';
      top: 0;
    }
  }

  .rt-tbody,
  .rt-tfoot {
    color: ${props => props.theme.gray.gray400};
  }

  .rt-thead {
    box-shadow: none;

    .rt-tr {
      .rt-th {
        border: 0;
        font-size: ${props => props.theme.font.sizes.s};
        font-weight: 500;
        padding: 36px 10px;
      }
    }
  }

  .rt-tbody {
    box-shadow: none;
    overflow-x: hidden;
    overflow-y: auto;
    flex-direction: column;

    .rt-tr-group {
      border: none;
      border-bottom: 1px solid ${props => props.theme.border.primary} !important;

      .rt-tr {
        background: ${props => props.theme.background.primary};

        &:hover {
          background: ${props => props.theme.background.secondary} !important;
        }

        .rt-td {
          border: 0;
          padding: 8px 10px 16px;

          a {
            color: ${props => props.theme.text.primary};
          }
        }
      }
    }
  }

  .rt-noData {
    background: ${props => props.theme.background.primary};
    color: ${props => props.theme.text.primary};
  }

  .-loading {
    background: ${props => props.theme.background.primary};
    position: absolute;
    top: 0;
    color: white;
    left: 0;
    right: 0;
    bottom: 0;
    display: flex;
    justify-content: center;
    align-items: center;
    display: none;
  }

  .-loading > div {
    color: ${props => props.theme.text.primary};
  }

  .rt-tfoot {
    box-shadow: none;

    .rt-td {
      border: 0;
    }
  }
`;

export type DataGridProps<RowType = any> = {
  columns: DataGridColumnProps[];
  data: RowType[];
} & Partial<Omit<TableProps, 'columns' | 'data'>>;

export function DataGrid<RowType = any>({
  columns,
  data,
  defaultPageSize = 10,
  pageSizeOptions = [10, 20],
  pageSize,
  TheadComponent,
  ...props
}: DataGridProps<RowType>) {
  const getColumnType = (column: DataGridColumnProps) => {
    if (column.type === ColumnType.Icon) {
      return (cellData: CellInfo) =>
        IconCell(column as IconCellProps, cellData.original);
    }

    if (column.type === ColumnType.RangeColor) {
      return (cellData: CellInfo) =>
        RangeCell(column as RangeCellProps, cellData.value);
    }

    if (column.type === ColumnType.CustomCell) {
      return column.cell;
    }

    return undefined;
  };

  // Map columns to react-table column prop
  const mappedColumns = columns.map((column: DataGridColumnProps) => ({
    accessor: column.key,
    Header: column.header,
    className: column.className,
    width: column.width,
    Cell: getColumnType(column),
  }));

  const minRows = pageSize || defaultPageSize;

  const tableProps = {
    ...props,
    data,
    columns: mappedColumns,
    defaultPageSize,
    pageSize,
    pageSizeOptions,
    minRows: data.length >= minRows ? minRows : data.length,
    PaginationComponent: DataGridPagination,
    TheadComponent: columns.every(item => !item.header)
      ? () => null
      : TheadComponent,
    getProps: () => ({ id: 'my-table-id' }),
  };

  return <TableContainer {...tableProps} />;
}
